#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after(struct block_header const* block) {
    return  (void*)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(
    struct block_header const* fst,
    struct block_header const* snd) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {

    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {

    if (block == NULL || block->next == NULL || !mergeable(block, block->next)) {
        return false;
    }

    size_t size_of_both = size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes;
    block_init(block, (block_size) { .bytes = size_of_both }, block->next->next);
    return true;
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    block_size real_size = (block_size){ .bytes = region_actual_size(size_from_capacity((block_capacity) { .bytes = query }).bytes) };
    void* received_region = map_pages(addr, real_size.bytes, MAP_FIXED_NOREPLACE);
    if (received_region == MAP_FAILED) {
        received_region = map_pages(addr, real_size.bytes, 0);
        if (received_region == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    block_init(received_region, real_size, NULL);
    return (struct region) {
        .extends = (received_region == addr),
        .addr = received_region,
        .size = real_size.bytes
    };
}

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    struct block_header* current_block = (struct block_header*)HEAP_START;
    size_t size;
    struct block_header* tmp_block;
    
    while (current_block != NULL) {
        size = size_from_capacity(current_block->capacity).bytes;
        while (blocks_continuous(current_block, current_block->next)) {
            if (current_block->next == NULL) {
                break;
            }
            size += size_from_capacity(current_block->next->capacity).bytes;
            current_block = current_block->next;
        }
        tmp_block = current_block->next;
        munmap(current_block, size);
        current_block = tmp_block;

    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    query = size_max(BLOCK_MIN_CAPACITY, query);
    if (block == NULL || !block_splittable(block, query)) {
        return false;
    }

    block_capacity new_capacity = (block_capacity){ .bytes = query };
    block_size size_of_first_block = size_from_capacity(new_capacity);
    block_size size_of_second_block = (block_size){ .bytes = block->capacity.bytes - query};

    void* next_block = block->next;

    block_init(block, size_of_first_block, block->contents + query);
    block_init(block->contents + query, size_of_second_block, next_block);
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if (!block) return (struct block_search_result) { .type = BSR_CORRUPTED};

    struct block_header* result = NULL;

    while (block != NULL) {
        while (try_merge_with_next(block)) {}
        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {
                .block = block,
                .type = BSR_FOUND_GOOD_BLOCK
            };
        }
        result = block;
        block = block->next;
    }
    return (struct block_search_result) {
        .block = result,
        .type = BSR_REACHED_END_NOT_FOUND
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result block_result = find_good_or_last(block, query);
    if (block_result.type != BSR_FOUND_GOOD_BLOCK) {
        return block_result;
    }

    split_if_too_big(block_result.block, query);
    block_result.block->is_free = false;
    return block_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if (last == NULL) { return NULL; }

    const void* end_addr = (void *)block_after(last);
    struct region grown_region = alloc_region(end_addr, query);

    if (region_is_invalid(&grown_region)) return NULL;

    block_size new_size = (block_size){ .bytes = grown_region.size };
    block_init(grown_region.addr, new_size, NULL);
    last->next = grown_region.addr;

    if (try_merge_with_next(last)) {
        return last;
    }

    return grown_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

    if (heap_start == NULL) {
        return NULL;
    }

    size_t fixed_query = size_max(BLOCK_MIN_CAPACITY, query);

    struct block_search_result search_result = try_memalloc_existing(fixed_query, heap_start);

    if (search_result.type == BSR_REACHED_END_NOT_FOUND) {
        struct block_header* new_block;
        if ((new_block = grow_heap(search_result.block, fixed_query)) == NULL) {
            return NULL;
        }
        return try_memalloc_existing(fixed_query, new_block).block;
    }

    return search_result.block;
}

void* _malloc( size_t query ) {
    struct block_header* block = (struct block_header*)HEAP_START;
    block->next = NULL;
  struct block_header* const addr = memalloc( query, block );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
