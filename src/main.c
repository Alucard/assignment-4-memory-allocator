#include "mem.h"
#include <assert.h>
#include <inttypes.h>
#include <stdio.h>


#define HEAP_SIZE 10000
#define MEMORY_SIZE 30
#define MEMORY_LEN 500
#define MEMORY_TO_OVERFLOW 15000


struct my_struct {
    int64_t value;
    char* name;
    bool flag;
};

static void default_successful_allocation() {
    printf("\n... test default_successful_allocation_test started ...\n");

    void* new_heap = heap_init(HEAP_SIZE);
    debug_heap(stderr, new_heap);

    void* memory = _malloc(MEMORY_SIZE);
    assert(memory);

    struct my_struct* this_struct = (struct my_struct*)_malloc(sizeof(struct my_struct));
    assert(this_struct);

    debug_heap(stderr, new_heap);

    heap_term();

    printf("... test default_successful_allocation_test ended successfully ...\n");
}

static void free_one_block_from_allocated() {
    printf("\n... test free_one_block_from_allocated started ...\n");

    void* new_heap = heap_init(HEAP_SIZE);
    debug_heap(stderr, new_heap);

    _malloc(sizeof(struct my_struct));
    struct my_struct* my_struct = (struct my_struct*)_malloc(sizeof(struct my_struct));
    debug_heap(stderr, new_heap);

    _free(my_struct);
    debug_heap(stderr, new_heap);

    heap_term();

    printf("... test free_one_block_from_allocated ended successfully ...\n");
}

static void free_two_blocks_from_allocated() {
    printf("\n... test free_two_blocks_from_allocated started ...\n");

    void* new_heap = heap_init(HEAP_SIZE);
    debug_heap(stderr, new_heap);

    _malloc(sizeof(struct my_struct));
    struct my_struct* my_struct1 = (struct my_struct*)_malloc(sizeof(struct my_struct));
    struct my_struct* my_struct2 = (struct my_struct*)_malloc(sizeof(struct my_struct));
    debug_heap(stderr, new_heap);

    _free(my_struct1);
    _free(my_struct2);
    debug_heap(stderr, new_heap);

    heap_term();

    printf("... test free_two_blocks_from_allocated ended successfully ...\n");
}

static void memory_ended_new_region_extends_old() {
    printf("\n... test memory_ended_new_region_extends_old started ...\n");

    void* new_heap = heap_init(HEAP_SIZE);
    debug_heap(stderr, new_heap);

    _malloc(HEAP_SIZE * 1.5);
    debug_heap(stderr, new_heap);

    heap_term();

    printf("... test memory_ended_new_region_extends_old ended successfully ...\n");
}

static void memory_ended_new_region_in_another_space() {
    printf("\n... test memory_ended_new_region_in_another_space started...\n");

    void* new_heap = heap_init(HEAP_SIZE);
    void* allocated_memory = mmap(new_heap + HEAP_SIZE, MEMORY_LEN, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    debug_heap(stderr, new_heap);

    _malloc(MEMORY_TO_OVERFLOW / 2);
    _malloc(MEMORY_TO_OVERFLOW / 2);
    debug_heap(stderr, new_heap);

    munmap(allocated_memory, MEMORY_LEN);
    heap_term();

    printf("... test memory_ended_new_region_in_another_space ended successfully ...\n");
}

int main() {
    default_successful_allocation();
    free_one_block_from_allocated();
    free_two_blocks_from_allocated();
    memory_ended_new_region_extends_old();
    memory_ended_new_region_in_another_space();
    return 0;
}
